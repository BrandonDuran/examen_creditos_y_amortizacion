const monto=20000;
const plazo=12;
const interes=.12;
const llenarTabla=document.querySelector('#lista-tabla tbody');

calcular(monto,plazo,interes);
function calcular(monto,plazo,interes){
    let fechainicio=[];
    let fechafin=[];
    let FechaI=moment('2021-08-03');
    let FechaF=moment('2021-09-03');
    let periodo=0,intereses=0, saldoInsoluto=monto,amortizacion=0,iva=0,flujo=0;
    
    amortizacion=monto/plazo;
    
    for(let i=1;i<plazo+1;i++){
       if (i<=1){
       saldoInsoluto=monto;
       }else {
        saldoInsoluto=saldoInsoluto-amortizacion;
    }
        periodo=periodo+1;
        //calcula las fechas
        fechainicio[i]=FechaI.format('DD-MM-YYYY');
        fechafin[i]=FechaF.format('DD-MM-YYYY');
        dias=(FechaF.diff(FechaI,'days')); //Calcula los dias entre los meses
        FechaI.add(1,'month'); //Aumenta el mes
        FechaF.add(1, 'month');
       
        intereses=saldoInsoluto*dias*(interes/360);
        iva=intereses*.16;
        flujo=amortizacion+intereses+iva;

        //Creacion de las filas en la tabla
        const row=document.createElement('tr')
        row.innerHTML=`
            <td>${periodo}</td>
            <td>${fechainicio[i]}</td>
            <td>${fechafin[i]}</td>
            <td>${dias}</td>
            <td></td>
            <td>${saldoInsoluto.toFixed(2)}</td>
            <td>${amortizacion.toFixed(2)}</td>
            <td>${intereses.toFixed(2)}</td>
            <td>${iva.toFixed(2)}</td>
            <td>${flujo.toFixed(2)}</td>
        `;
        llenarTabla.appendChild(row);
        
    }

    


}

/*i=s*dp*(T/360)
i=interes ordinario aKa intereses
s=Saldo insoluto
Dp=dias correspondientes al periodo en curso
T=Tasa de interes anual*/
